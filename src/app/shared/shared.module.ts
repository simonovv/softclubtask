import { NgModule } from '@angular/core';

import { AppLogoComponent } from './app-logo/app-logo.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppLogoComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    AppLogoComponent,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
