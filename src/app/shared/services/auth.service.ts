import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ClientInfoService } from './clientInfo.service';

const config = {
  apiUrl: 'https://192.168.253.168:7703'
};

@Injectable()
export class AuthService {
  private isAuth = false;

  constructor(private http: Http, private clientInfoService: ClientInfoService) { }

  isAuthunticate(): boolean {
    this.checkAuth();
    return this.isAuth;
  }

  checkAuth() {
    if (localStorage.getItem('user')) {
      this.isAuth = true;
      return true;
    }
    return false;
  }

  getUserByEmail(email: string): Observable<any> {
    return this.http.get(`http://localhost:3000/users?email=${email}`)
      .pipe(
        map((response: Response) => {
          this.isAuth = true;
          return response.json();
        }
      ));
  }

  login(login: string, password: string) {
    return this.http.post(`${config.apiUrl}/ibservices/session/login`, {
      deviceUDID: 'web',
      applicID: 1,
      clientKind: 5,
      login: login,
      password: password,
      browser: this.clientInfoService.getBrowserInfo().name,
      browserVersion: this.clientInfoService.getBrowserInfo().version,
      platform: this.clientInfoService.getOsName(),
      clientTimeZone: new Date().getTimezoneOffset() * -1
    }).pipe(map(user => {
      if (user) {
        localStorage.setItem('sessionToken', JSON.parse(user._body).sessionToken);
        localStorage.setItem('user', JSON.stringify(user));
        this.isAuth = true;
      }

      return user;
    }));
  }

  logout() {
    this.isAuth = false;
    window.localStorage.removeItem('user');
  }
}
