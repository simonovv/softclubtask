export class ClientInfoService {
  getBrowserInfo() {
    const ua = navigator.userAgent;
    let tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return {name: 'IE ', version: (tem[1] || '')};
        }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/);
        if (tem != null) {return {name: 'Opera', version: tem[1]}; }
        }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {M.splice(1, 1, tem[1]); }
    return {
      name: M[0],
      version: M[1]
    };
  }

  getOsName() {
    let OSName = 'Unknown';
    if (window.navigator.userAgent.indexOf('Windows NT 10.0') !== -1) { OSName = 'Windows 10'; }
    if (window.navigator.userAgent.indexOf('Windows NT 6.2')  !== -1) { OSName = 'Windows 8'; }
    if (window.navigator.userAgent.indexOf('Windows NT 6.1')  !== -1) { OSName = 'Windows 7'; }
    if (window.navigator.userAgent.indexOf('Windows NT 6.0')  !== -1) { OSName = 'Windows Vista'; }
    if (window.navigator.userAgent.indexOf('Windows NT 5.1')  !== -1) { OSName = 'Windows XP'; }
    if (window.navigator.userAgent.indexOf('Windows NT 5.0')  !== -1) { OSName = 'Windows 2000'; }
    if (window.navigator.userAgent.indexOf('Mac')             !== -1) { OSName = 'Mac/iOS'; }
    if (window.navigator.userAgent.indexOf('X11')             !== -1) { OSName = 'UNIX'; }
    if (window.navigator.userAgent.indexOf('Linux')           !== -1) { OSName = 'Linux'; }
    return OSName;
  }
}
