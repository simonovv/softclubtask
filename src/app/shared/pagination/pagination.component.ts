import { Component, Input, OnInit, Output, EventEmitter, Renderer2, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, AfterViewInit {
  @Input() itemsPerPage: number;
  @Input() curPage: number;
  @Input() totalItems: number;
  @Output() pageChange = new EventEmitter<{paginationStart: number, paginationEnd: number}>();
  paginationElements: NodeListOf<Element>;
  paginationStart: number;
  paginationEnd: number;
  pages = [];

  constructor(private renderer: Renderer2) {}

  ngAfterViewInit() {
    this.paginationElements = document.querySelectorAll('.pagination-element');
    if (this.paginationElements.length !== 0) {
      this.renderer.addClass(this.paginationElements[0], 'active');
    }
  }

  ngOnInit() {
    this.pages = Array(Math.ceil(this.totalItems / +this.itemsPerPage));
    this.paginationStart = ((this.curPage - 1) * +this.itemsPerPage);
    this.paginationEnd = this.paginationStart + +this.itemsPerPage;
    this.pageChange.emit({paginationStart: this.paginationStart, paginationEnd: this.paginationEnd});
  }

  onPageChange(page: number) {
    this.renderer.removeClass(this.paginationElements[this.curPage - 1], 'active');
    this.renderer.addClass(this.paginationElements[page - 1], 'active');
    this.curPage = page;
    this.paginationStart = ((this.curPage - 1) * +this.itemsPerPage);
    this.paginationEnd = this.paginationStart + +this.itemsPerPage;
    this.pageChange.emit({paginationStart: this.paginationStart, paginationEnd: this.paginationEnd});
  }
}
