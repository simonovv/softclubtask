import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appInputHelper]'
})
export class MoveInputHelperDirective {

  constructor(private element: ElementRef, private renderer: Renderer2) { }

  @HostListener('focus') onFocus() {
    if (this.element.nativeElement.value === '0') {
      this.element.nativeElement.value = '';
    }
  }

  @HostListener('focusout') onFocusOut() {
    if (this.element.nativeElement.value === '') {
      this.element.nativeElement.value = 0;
    }
  }
}
