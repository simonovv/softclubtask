import { Component, OnInit, Renderer2 } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { TestInputComponent } from '../dynamic-table/filters/select-input/test-input.component';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css']
})
export class ScoreComponent implements OnInit {

  constructor(private http: Http, private renderer: Renderer2) { }

  headerConfig = [
    {field: 'name', component: TestInputComponent},
    {field: 'score', component: ''}
  ];
  itemsPerPage = 5;
  data: [];
  filters = new Map<string, string>();
  filteredData;
  headerElements = [];
  paginationStart = 0;
  paginationEnd = this.itemsPerPage;

  ngOnInit() {
    this.http.get('http://localhost:3000/info')
      .pipe(map(res => res.json()))
      .subscribe(data => {
        this.data = data;
      });


    this.headerElements = ['name', 'score', 'country'];
  }

  onPageChange(event) {
    this.paginationStart = event.paginationStart;
    this.paginationEnd = event.paginationEnd;
  }

  onSortData(event) {
    const data = this.filteredData ? this.filteredData : this.data;
    if (event.sortType) {
      data.sort((a: {}, b: {}) => {
        if (a[event.fieldName] < b[event.fieldName]) {
          return -1;
        } else if (a[event.fieldName] > b[event.fieldName]) {
          return 1;
        } else {
          return 0;
        }
      });
    } else {
      data.sort((a: {}, b: {}) => {
        if (a[event.fieldName] < b[event.fieldName]) {
          return 1;
        } else if (a[event.fieldName] > b[event.fieldName]) {
          return -1;
        } else {
          return 0;
        }
      });
    }
  }

  filter(keys) {
    this.filteredData =  this.data.filter(function(val: {}) {
      for (let i = 0; i < keys.length; i++) {
        if (!val[keys[i][0]].toString().includes(keys[i][1].toString())) {
          return false;
        }
      }
      return true;
    });
  }

  onFilter(event: {}) {
    const keys = Object.keys(event);
    keys.forEach(el => {
      if (event[el] !== null) {
        this.filters.set(el, event[el]);
      }
    });
    this.filter(Array.from(this.filters));
  }
}
