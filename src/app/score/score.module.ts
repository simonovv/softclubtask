import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicTableComponent } from '../dynamic-table/dynamic-table.component';
import { SharedModule } from '../shared/shared.module';
import { ScoreComponent } from './score.component';
import { KeysPipe } from '../dynamic-table/pipes/keys.pipe';
import { UcFirstPipe } from '../dynamic-table/pipes/ucFirst.pipe';
import { SortBy } from '../dynamic-table/pipes/sortBy.pipe';
import { ScoreRoutingModule } from './score-routing.module';
import { PaginationComponent } from '../shared/pagination/pagination.component';
import { TableFilterDirective } from '../dynamic-table/directives/table-filter.directive';
import { TestInputComponent } from '../dynamic-table/filters/select-input/test-input.component';
import { DefaultInputComponent } from '../dynamic-table/filters/default-input/default-input.component';
import { TableElementDirective } from '../dynamic-table/directives/table-element.directive';

import { DefaultElementComponent } from '../dynamic-table/directives/table-element.directive';
import { TestFieldComponent } from '../dynamic-table/fields/testField.component';
import { DynamicTableModule } from '../dynamic-table/dynamic-table.module';

@NgModule({
  declarations: [
    ScoreComponent,
    PaginationComponent,
    TestInputComponent,
    DefaultInputComponent,
    DefaultElementComponent,
    TestFieldComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ScoreRoutingModule,
    DynamicTableModule
  ],
  entryComponents: [TestInputComponent, DefaultInputComponent, DefaultElementComponent, TestFieldComponent]
})
export class ScoreModule { }
