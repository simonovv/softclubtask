import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import urlConfig from '../config/urls-config';

@Injectable()
export class BankInfoService {
  constructor(private http: Http) { }

  getTableData(sort = [], filter = [], currPage = 1) {
    return this.http.post(`${urlConfig.tableUrl}/ibservices/document/getDocumentsGrid`, {
      sort: {},
      sortList: sort,
      filterLike: filter,
      filterIntervalList: [],
      filterDateIntervalList: [],
      numberOfPage: currPage,
      itemsPerPage: 20
    }).pipe(map((res: Response) => res.json()));
  }
}
