import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { FormControl, FormGroup } from '@angular/forms';
import { BankInfoService } from './bank-info.service';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  templateUrl: './bank-info.component.html',
  styleUrls: ['./bank-info.component.css'],
  providers: [BankInfoService]
})
export class BankInfoComponent implements OnInit {

  currentPage = 1;
  form: FormGroup;
  filterLike = [];
  sortList = [];
  // tableData: [];
  tableData = [];
  itemsPerPage = 20;

  constructor(private http: Http, private infoService: BankInfoService, public translate: TranslateModule) { }

  ngOnInit() {
    this.form = new FormGroup({
      'allInOne': new FormControl(''),
      'statusId': new FormControl('')
    });
    this.form.valueChanges.subscribe(value => {
      this.filter(value);
    });
    this.infoService.getTableData(this.sortList, this.filterLike, this.currentPage).subscribe(data => {
      this.tableData = data;
    });
  }

  filter(values: string) {
    const filterLike = [];
    const keys = Object.keys(values);
    keys.forEach((el, i) => {
      if (filterLike[el] && values[el] === '') {
        filterLike.splice(i, 1);
      }
      if (values[el]) {
        if (el === 'statusId') {
          filterLike.push({columnName: el, columnValueList: [values[el]]});
        } else {
          filterLike.push({columnName: el, columnValue: values[el]});
        }
      }
    });
    this.filterLike = filterLike;
    this.infoService.getTableData(this.sortList, filterLike, this.currentPage).subscribe(data => {
      this.tableData = data;
    });
  }

  onPageChange(event) {

  }

  onSort(event) {
    const sortList = [];
    sortList.push({
      columnName: event.fieldName, // switch to fieldName from event
      columnValue: event.sortType ? 'asc' : 'desc' // switch to type from event
    });
    this.sortList = sortList;
    this.infoService.getTableData(this.sortList, this.filterLike, this.currentPage).subscribe(data => {
      this.tableData = data;
    });
  }
}
