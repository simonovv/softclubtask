import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BankInfoRoutingModule } from './bank-info-routing.module';
import { BankInfoComponent } from './bank-info.component';
import { SharedModule } from '../shared/shared.module';
import { DynamicTableModule } from '../dynamic-table/dynamic-table.module';
import { PaginationComponent } from '../shared/pagination/pagination.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    BankInfoComponent,
    PaginationComponent
  ],
  imports: [
    CommonModule,
    BankInfoRoutingModule,
    SharedModule,
    DynamicTableModule,
    TranslateModule
  ],
})
export class BankInfoModule { }
