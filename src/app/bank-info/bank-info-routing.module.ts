import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BankInfoComponent } from './bank-info.component';


const routes: Routes = [
  {path: '', component: BankInfoComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BankInfoRoutingModule { }
