import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appCurr]'
})
export class CurrencyDirective {
  type = true;
  element: any;
  selectionStart: number;
  spaceCount = 0;
  key: number;
  maxLength = 9;
  constructor(el: ElementRef) {
    this.element = el;
  }

  @HostListener('input')
  onInput() {
    const value = this.element.nativeElement.value.split('');
    const splitedByDot = this.element.nativeElement.value.split('.');
    if (this.element.nativeElement.value.length <= 3) {
      if (!splitedByDot[1]) {
        this.element.nativeElement.value = '0.00';
      } else {
        this.element.nativeElement.value = '0' + '.' + splitedByDot[1];
      }
      if (this.key === 46) {
        this.element.nativeElement.setSelectionRange(1, 1);
      }
    }
    if (splitedByDot[1]) {
      if (splitedByDot[1].length === 1) {
        console.log(splitedByDot);
        this.element.nativeElement.value = splitedByDot[0] + '.' + splitedByDot[1] + '0';
      }
    }
    this.selectionStart = this.element.nativeElement.selectionStart;
    if (this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 2) {
      this.element.nativeElement.value = value.slice(0, -1).join('');
      this.element.nativeElement.setSelectionRange(
        this.element.nativeElement.value.length - 1,
        this.element.nativeElement.value.length - 1
      );
      return;
    }
    if (this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 1) {
      console.log(6546876867);
      this.element.nativeElement.value = this.element.nativeElement.value.slice(0, -1);
      this.element.nativeElement.setSelectionRange(
        this.element.nativeElement.value.length,
        this.element.nativeElement.value.length
      );
    }
    if (this.element.nativeElement.value.length - this.element.nativeElement.selectionStart > 2) {
      splitedByDot[0] = splitedByDot[0].replace(/ /g, '');
      const prevValue = splitedByDot[0];
      splitedByDot[0] = +splitedByDot[0] + '';
      if (splitedByDot[0].length !== prevValue.length) {
        this.selectionStart -= 1;
      }
      const tmp = splitedByDot[0].replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
      this.element.nativeElement.value = splitedByDot[0].replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ') + '.' + splitedByDot[1];
      this.element.nativeElement.value = tmp + '.' + splitedByDot[1];
      if (tmp.match(/ /g)) {
        if (this.spaceCount !== tmp.match(/ /g).length) {
          this.spaceCount = tmp.match(/ /g).length;
          if (this.key === 8) {
            this.selectionStart -= 1;
          } else {
            this.selectionStart += 1;
          }
        }
      }
      this.element.nativeElement.setSelectionRange(this.selectionStart, this.selectionStart);
    }
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(e: KeyboardEvent) {
    this.key = e.keyCode;

    if (
      (e.keyCode === 65 && e.ctrlKey === true) || // Allow: Ctrl+A
      (e.keyCode === 67 && e.ctrlKey === true) || // Allow: Ctrl+C
      (e.keyCode === 86 && e.ctrlKey === true) || // Allow: Ctrl+V
      (e.keyCode === 88 && e.ctrlKey === true) || // Allow: Ctrl+X
      (e.keyCode === 65 && e.metaKey === true) || // Cmd+A (Mac)
      (e.keyCode === 67 && e.metaKey === true) || // Cmd+C (Mac)
      (e.keyCode === 86 && e.metaKey === true) || // Cmd+V (Mac)
      (e.keyCode === 88 && e.metaKey === true) || // Cmd+X (Mac)
      (e.keyCode >= 35 && e.keyCode <= 39)
    ) {
      console.log('RETURN');
      return;
    }
    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
      (e.keyCode < 96 || e.keyCode > 105) &&
      (e.keyCode !== 8 && e.keyCode !== 46) &&
      e.keyCode !== 116
    ) {
      console.log('PREVENT');
      e.preventDefault();
      return;
    }
    const splitedByDot = this.element.nativeElement.value.split('.');
    if (e.keyCode === 46 &&
      (this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 3 ||
        this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 2)) {
      e.preventDefault();
      console.log('999');
      console.log(splitedByDot);
      const leftSide = splitedByDot[1].split('');
      console.log(leftSide);
      leftSide[1] = leftSide[0];
      leftSide[0] = '0';
      console.log(leftSide);
      this.element.nativeElement.value = splitedByDot[0] + '.' + leftSide.join('');
      this.element.nativeElement.setSelectionRange(
        this.element.nativeElement.value.length - 1,
        this.element.nativeElement.value.length - 1
      );
    }
    if (this.element.nativeElement.selectionStart === 0 && splitedByDot[0]['0'] === '0') {
      console.log('fdsfdsfdsfds');
      if (splitedByDot[0]['0'] === '0' && e.keyCode === 46) {
        e.preventDefault();
        this.element.nativeElement.value = '0' + '.' + splitedByDot[1];
      } else {
        this.element.nativeElement.value = '.' + splitedByDot[1];
      }
      this.element.nativeElement.setSelectionRange(0, 0);
    }
    if (e.keyCode === 46 &&
      (this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 2)
    ) {
      console.log(534534534);
      e.preventDefault();
       const leftSide = splitedByDot[1].split('');
       leftSide[1] = '0';
       this.element.nativeElement.value = splitedByDot[0] + '.' + leftSide.join('');
       this.element.nativeElement.setSelectionRange(
         this.element.nativeElement.value.length - 1,
         this.element.nativeElement.value.length - 1
       );
    }
    if (
      this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 2 &&
      ((e.keyCode === 8 || e.keyCode === 46) || e.keyCode === 46)
    ) {
      console.log(2);
      e.preventDefault();
      const value = this.element.nativeElement.value.split('.');
      if (value[0].length === 1) {
        this.element.nativeElement.value = 0 + '.' + value[1];
        this.element.nativeElement.setSelectionRange(
          this.element.nativeElement.value.length - 2,
          this.element.nativeElement.value.length - 2
        );
      } else {
        console.log(3);
        let tmp = value[0].replace(/ /g, '').slice(0, -1);
        tmp = tmp.replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
        this.element.nativeElement.value = tmp + '.' + value[1];
        this.element.nativeElement.setSelectionRange(
          this.element.nativeElement.value.length - 2,
          this.element.nativeElement.value.length - 2
        );
      }
    }
    if (
      (splitedByDot[0].replace(/ /g, '').length > this.maxLength - 1 &&
      (+e.keyCode !== 8) &&
      (+e.keyCode !== 46) &&
      !(this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 2 ||
      this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 1))
    ) {
      console.log('fdmsflkdsjl');
      e.preventDefault();
    }
    if (
      (this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 1 ||
      this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 0) &&
      (e.keyCode === 8)
    ) {
      console.log(5);
      e.preventDefault();
      const tmp = this.element.nativeElement.value.length - this.element.nativeElement.selectionStart;
      const value = this.element.nativeElement.value.split('');
      value[value.length - ((this.element.nativeElement.value.length - this.element.nativeElement.selectionStart)) - 1] = '0';
      this.element.nativeElement.value = value.join('');
      this.element.nativeElement.setSelectionRange(
        this.element.nativeElement.value.length - tmp - 1,
        this.element.nativeElement.value.length - tmp - 1
      );
    }
    if (this.element.nativeElement.value.length - this.element.nativeElement.selectionStart === 0) {
      e.preventDefault();
    }
  }
}
