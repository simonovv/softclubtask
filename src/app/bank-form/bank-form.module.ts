import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankInfoComponent } from './bank-form.component';
import { SharedModule } from '../shared/shared.module';
import { CurrencyDirective } from './currency.directive';

@NgModule({
  declarations: [
    BankInfoComponent,
    CurrencyDirective
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
})
export class BankInfoModule { }
