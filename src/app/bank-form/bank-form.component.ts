import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  templateUrl: './bank-form.component.html',
  styleUrls: ['./bank-form.component.css']
})
export class BankInfoComponent implements OnInit {
  form: FormGroup;
  validatorsConfig = [
    {field: 'BLR', validators: [
      {field: 'identityNumber', validators: {maxlength: 2}}
    ]},
    {field: 'GB', validators: [
        {field: 'identityNumber', validators: {maxlength: 5}}
      ]},
    {field: 'USA', validators: [
        {field: 'identityNumber', validators: {maxlength: 10}}
      ]},
    {field: 'UA', validators: [
        {field: 'identityNumber', validators: {maxlength: 20}}
      ]}
  ];
  errorList = [
    {error: 'maxlength', message: 'Identity need to be less than '},
    {error: 'required', message: 'Identity is required'}
  ];
  errors = [];

  ngOnInit() {
    this.form = new FormGroup({
      'name': new FormControl(''),
      'date': new FormControl(''),
      'identityNumber': new FormControl(''),
      'passwordNumber': new FormControl(''),
      'country': new FormControl(),
      'sum': new FormControl('0.00')
    });
    this.onCountryChange('BLR');
    this.getValidationErrors();
    this.form.valueChanges.subscribe((el) => {
      this.getValidationErrors();
    });
    this.form.get('country').valueChanges.subscribe(el => {
      this.onCountryChange(el);
    });
  }

  onClick() {
    this.getValidationErrors();
    console.log(this.errors);
    console.log(this.form);
  }

  onCountryChange(value) {
    console.log(value);
    const a = [];
    this.validatorsConfig.forEach(el => {
      a.push(el['field']);
    });
    this.validatorsConfig.forEach((el: {}) => {
      el['validators'].forEach(validator => {
        if (a.includes(value)) {
          if (el['field'] === value) {
            console.log(this.getValidators(validator['validators']));
            this.form.get(validator['field']).setValidators(this.getValidators(validator['validators']));
            this.form.get(validator['field']).updateValueAndValidity();
          }
        } else {
          console.log('NOT INCLUDES');
          this.form.get(validator['field']).clearValidators();
          this.form.get(validator['field']).updateValueAndValidity();
        }
      });
    });
  }

  getValidationErrors() {
      const errors = this.form.get('identityNumber')['errors'];
      console.log(errors);
      let keys = [];
      const err = [];
      if (errors) {
        keys = Object.keys(errors);
        this.errorList.forEach((el) => {
          keys.forEach(key => {
            console.log(key);
            if (key === el['error']) {
              if (key === 'maxlength') {
                err.push(el['message'] + this.form.get('identityNumber')['errors'][key]['requiredLength']);
              } else if (key === 'required') {
                err.push(el['message']);
              }
            }
          });
        });
      }
      this.errors = err;
  }

  getValidators(validators) {
    const outValidators = [Validators.required];
    const keys = Object.keys(validators);
    keys.forEach(el => {
      switch (el) {
        case 'maxlength': outValidators.push(Validators.maxLength(validators[el]));
      }
    });
    return outValidators;
  }
}
