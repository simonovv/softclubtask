import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard-layout/dashboard.component';
import { AppComponent } from './app.component';

import routesConfig from '../environments/routes';
import { AuthGuard } from './auth/auth-guard.service';
import { BankInfoComponent } from './bank-form/bank-form.component';

const routes: Routes = [
  {path: routesConfig.dashboard, component: DashboardComponent},
  {path: routesConfig.newPlayer, canActivate: [AuthGuard], loadChildren: './add-player/add-player.module#AddPlayerModule'},
  {path: routesConfig.game501, canActivate: [AuthGuard], loadChildren: './game-501/game-501.module#Game501Module'},
  {path: routesConfig.score, loadChildren: './score/score.module#ScoreModule'},
  {path: 'bank-info', canActivate: [AuthGuard], loadChildren: './bank-info/bank-info.module#BankInfoModule'},
  {path: '', component: AppComponent},
  {path: 'bank-form', component: BankInfoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
