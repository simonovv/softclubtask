import { Player } from './player.model';

export class PlayerService {
  private players: Player[] = [];

  getAllPlayers() {
    return this.players;
  }

  // need validation to players with same name
  addPlayer(player: Player) {
    this.players.push(player);
  }

  deletePlayer(id: number) {
    this.players.splice(id, 1);
  }
}
