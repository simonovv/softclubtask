import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, Http } from '@angular/http';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlayerService } from './player/player.service';
import { Game501Service } from './game-501/game-501.service';
import { DashboardModule } from './dashboard/dashboard.module';
import { AuthModule } from './auth/auth.module';
import { AuthService } from './shared/services/auth.service';
import { AuthGuard } from './auth/auth-guard.service';
import { ClientInfoService } from './shared/services/clientInfo.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BankInfoModule } from './bank-form/bank-form.module';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    DashboardModule,
    AuthModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
      }
    }),
    BankInfoModule
  ],
  providers: [PlayerService, Game501Service, AuthService, AuthGuard, ClientInfoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
