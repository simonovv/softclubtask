import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private router: Router, public translate: TranslateService) {
    translate.addLangs(['en', 'ru']);

    if (localStorage.getItem('lang')) {
      this.translate.use(localStorage.getItem('lang'));
    } else {
      const browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/en|ru/) ? browserLang : 'ru');
    }
  }
  title = 'SoftClubDartsTask';

  ngOnInit() {
    // this.router.navigate(['login']);
  }

  setLanguage(lang) {
    this.translate.use(lang);
    localStorage.setItem('lang', lang);
  }
}
