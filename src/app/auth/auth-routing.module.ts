import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth.component';

import { LoginGameComponent } from './login-game/login.component';
import { LoginBankComponent } from './login-bank/login-bank.component';

const routes: Routes = [
    {path: 'login', component: AuthComponent },
    {path: 'login/game', component: LoginGameComponent},
    {path: 'login/bank', component: LoginBankComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}
