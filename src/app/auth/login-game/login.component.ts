import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginGameComponent implements OnInit {
  form: FormGroup;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    if (this.authService.checkAuth()) {
      this.router.navigate(['dashboard']);
    }
    this.form = new FormGroup({
      'email': new FormControl(),
      'password': new FormControl()
    });
  }

  onGuest() {
    this.router.navigate(['/dashboard']);
  }

  onSubmit() {
    const formData = this.form.value;

    this.authService.getUserByEmail(formData.email)
      .subscribe(user => {
        if (user[0]) {
          if (user[0].password === formData.password) {
            // this.authService.login();
            localStorage.setItem('user', '12345');
            this.router.navigate(['/dashboard']);
          } else {
            alert('Incorect password');
          }
        } else {
          alert('User with current email doesnt exists');
        }
      });
  }
}
