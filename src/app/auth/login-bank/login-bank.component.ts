import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

@Component({
  templateUrl: './login-bank.component.html',
  styleUrls: ['./login-bank.component.css']
})
export class LoginBankComponent implements OnInit {
  form: FormGroup;

  constructor(private authService: AuthService, private router: Router, public translate: TranslateService) { }

  ngOnInit() {
    if (this.authService.checkAuth()) {
      this.router.navigate(['dashboard']);
    }
    this.form = new FormGroup({
      'login': new FormControl(),
      'password': new FormControl()
    });
  }

  onSubmit() {
    const formData = this.form.value;

    this.authService.login(formData.login, formData.password)
      .subscribe(data => {
        this.router.navigate(['bank-info']);
      },
      error => {
        alert(error);
      });
  }
}
