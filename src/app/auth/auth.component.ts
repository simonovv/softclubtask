import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-auth',
  template: `
    <div>
      <button (click)="redirectToLogin('game')">{{ 'LOGIN.LOGINGAME.SWITCH' | translate }}</button>
      <button (click)="redirectToLogin('bank')">{{ 'LOGIN.LOGINBANK.SWITCH' | translate }}</button>
    </div>
  `,
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  constructor(
    private router: Router,
    private active: ActivatedRoute,
    private authService: AuthService,
    public translate: TranslateService
  ) { }

  ngOnInit() {
    if (this.authService.checkAuth()) {
      this.router.navigate(['dashboard']);
    }
  }

  redirectToLogin(retirectTo: string) {
    this.router.navigate([retirectTo], {relativeTo: this.active});
  }
}
