import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-default-input',
  templateUrl: './default-input.component.html'
})
export class DefaultInputComponent implements OnInit {
  name: string;
  // form: FormGroup;
  control = new FormControl('');
  // inputGroup: FormGroup;

  // ngOnInit() {
  //   this.form = new FormGroup({
  //     [this.name]: new FormControl()
  //   });
  //   this.control = this.form.get(this.name);
  //   this.inputGroup.addControl(this.name, this.control);
  // }

  ngOnInit() {
  }

  stopPropogation() {
    event.stopPropagation();
  }
}
