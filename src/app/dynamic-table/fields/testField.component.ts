import { Component } from '@angular/core';

@Component({
  template: `
    <div>
      <p>{{ text }}</p>
      <img src="{{ imgSrc }}"/>
    </div>
  `
})
export class TestFieldComponent {
  imgSrc: string;
  text: string;
}
