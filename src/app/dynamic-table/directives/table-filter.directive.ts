import { Directive, Input, OnInit, ViewContainerRef, TemplateRef, ComponentFactoryResolver, ComponentRef } from '@angular/core';

import { FormGroup } from '@angular/forms';
import { DefaultInputComponent } from '../filters/default-input/default-input.component';

@Directive({
  selector: '[appTableFilter]'
})
export class TableFilterDirective implements OnInit {
  config: [];
  fieldName: string;
  form: FormGroup;
  inst: ComponentRef<{}>;

  @Input('appTableFilter')
  set appHeader(value) {
    this.form = value;
  }
  @Input()
  set appTableFilterName(value) {
    this.fieldName = value;
  }
  @Input()
  set appTableFilterConfig(value) {
    this.config = value;
  }

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef,
    private resolver: ComponentFactoryResolver
  ) { }

  ngOnInit() {
    this.generateComponent(DefaultInputComponent);
    if (this.config) {
      this.config.forEach(config => {
        if (config['field'] === this.fieldName) {
          this.destroyComponent();
          if (config['component']) {
            this.generateComponent(config['component']);
          }
        }
      });
    }
  }

  generateComponent(component: any) {
    this.viewContainerRef.createEmbeddedView(this.templateRef);
    this.inst = this.viewContainerRef.createComponent(this.resolver.resolveComponentFactory(component));
    this.form.addControl(this.fieldName, this.inst.instance['control']);
  }

  destroyComponent() {
    this.inst.destroy();
    this.form.removeControl(this.fieldName);
  }
}
