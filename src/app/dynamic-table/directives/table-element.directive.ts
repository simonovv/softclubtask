import { Directive, TemplateRef, ViewContainerRef, ComponentFactoryResolver, OnInit, ComponentRef, Input, Component } from '@angular/core';

@Component({
  template: '{{ data }}'
})
export class DefaultElementComponent {
  data: string;
}

@Directive({
  selector: '[appTableElement]'
})
export class TableElementDirective implements OnInit {
  inst: ComponentRef<{}>;
  config: [];
  elementData: {} | string;

  @Input('appTableElement')
  set appTableElementData(value) {
    this.elementData = value;
  }
  @Input()
  set appTableFilterConfig(value) {
    this.config = value;
  }

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef,
    private resolver: ComponentFactoryResolver
  ) { }

  ngOnInit() {
    if (this.elementData['component']) {
      this.generateComponent(this.elementData['component']);
    } else {
      this.generateComponent(DefaultElementComponent);
    }
  }

  generateComponent(component: any) {
    this.viewContainerRef.createEmbeddedView(this.templateRef);
    this.inst = this.viewContainerRef.createComponent(this.resolver.resolveComponentFactory(component));
    if (this.elementData['data']) {
      const keys = Object.keys(this.elementData['data']);
      keys.forEach(el => {
        this.inst.instance[el] = this.elementData['data'][el];
      });
    } else {
      this.inst.instance['data'] = this.elementData;
    }
  }
}
