import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableElementDirective } from '../dynamic-table/directives/table-element.directive';
import { DynamicTableComponent } from './dynamic-table.component';
import { KeysPipe } from './pipes/keys.pipe';
import { UcFirstPipe } from './pipes/ucFirst.pipe';
import { SortBy } from './pipes/sortBy.pipe';
import { TableFilterDirective } from './directives/table-filter.directive';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    DynamicTableComponent,
    KeysPipe,
    UcFirstPipe,
    SortBy,
    TableFilterDirective,
    TableElementDirective,
  ],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports: [
    DynamicTableComponent,
    KeysPipe,
    UcFirstPipe,
    SortBy,
    TableFilterDirective,
    TableElementDirective,
  ]
})
export class DynamicTableModule { }
