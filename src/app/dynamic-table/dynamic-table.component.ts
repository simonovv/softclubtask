import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TestFieldComponent } from './fields/testField.component';

@Component({
  selector: 'app-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.css']
})
export class DynamicTableComponent implements OnInit {
  @Input() header: string;
  @Input() headerElements = [];
  @Input() headerConfig = [];
  // TODO: @Input() data: [];
  @Input() data = [];
  @Output() sort = new EventEmitter<{fieldName: string, sortType: boolean}>();
  @Output() filter = new EventEmitter<{}>();
  outputValue: string;
  sortType: boolean;
  form: FormGroup;

  ngOnInit() {

    /*
    TODO: delete data for test
    */
    // this.data['0']['name'] = {
    //   component: TestFieldComponent,
    //   data: {
    //     imgSrc: 'http://www.google.com/images/branding/googleg/1x/googleg_standard_color_128dp.png',
    //     text: 'Google'
    //   },
    // };
    this.form = new FormGroup({});
    this.form.valueChanges.subscribe((el) => {
      this.filter.emit(el);
    });
  }

  onSort(fieldName: string) {
    this.sortType ? this.sortType = !this.sortType : this.sortType = true;
    this.sort.emit({fieldName, sortType: this.sortType});
  }

  stopPropogation() {
    event.stopPropagation();
  }
}
