import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ucFirst'
})
export class UcFirstPipe implements PipeTransform {
  transform(value: string, ...args: any[]) {
    if (!value) { return value; }

    return value[0].toUpperCase() + value.slice(1);
  }
}
