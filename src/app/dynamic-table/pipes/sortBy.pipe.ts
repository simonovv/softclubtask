import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortBy'
})
export class SortBy implements PipeTransform {
  transform(value: [], filedName: string, type: string) {
    if (!Array.isArray(value)) {
      return;
    }

    if (type === 'ASC' || !type) {
      value.sort((a: any, b: any) => {
        if (a[filedName] < b[filedName]) {
          return -1;
        } else if (a[filedName] > b[filedName]) {
          return 1;
        } else {
          return 0;
        }
      });
    } else {
      value.sort((a: any, b: any) => {
        if (a[filedName] < b[filedName]) {
          return 1;
        } else if (a[filedName] > b[filedName]) {
          return -1;
        } else {
          return 0;
        }
      });
    }

    return value;
  }
}
