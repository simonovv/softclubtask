import { Player } from '../player/player.model';
import config from '../../environments/config';
import { EventEmitter } from '@angular/core';

export class Game501Service {
  numberOfMoves: number = config.numberOfMoves;
  additionalMoves = false;
  winnerEvent = new EventEmitter<Player>();
  gameEvent = new EventEmitter<boolean>();
  isHaveWinner = false;
  moveScore: Map<Player, number> = new Map();
  scoreList = [];
  moveItems = new Map<Player, {}>();

  initGame(players: Player[]) {
    this.moveScore.clear();
    this.moveItems.clear();
    this.gameEvent.emit(true);
    this.isHaveWinner = false;
    players.forEach(el => this.moveScore.set(el, 501));
    this.scoreList.splice(0, this.scoreList.length);
    this.scoreList.push(this.moveScore);
  }

  handleMove(player: Player, moveItem: {}) {
    this.moveItems.set(player, moveItem);
  }

  getSumOfMove() {
    const map = new Map<Player, number>();
    this.moveItems.forEach((value, key) => {
      let sum = 0;
      const darts = Object.values(value);
      darts.forEach((el: { points: string; zone: number }) => {
        sum += +el.points * el.zone;
      });
      map.set(key, sum);
    });
    return map;
  }

  getWinner() {
    let winner: Player;
    const entreies = this.getLastMove().entries();
    let iterPlayer: Player;
    winner = entreies.next().value;
    for (let i = 1; i < this.getLastMove().length; i++) {
      iterPlayer = entreies.next().value;
      winner[1] < iterPlayer[1] ? (winner = iterPlayer) : (winner = winner);
    }
    return winner[0];
  }

  isDraw() {
    let sums = [];
    Array.from(this.getSumOfMove()).forEach((el: any) => sums.push(el[1]));
    sums = sums.filter(function(elem, pos, arr) {
      return pos !== arr.indexOf(elem) || pos !== arr.lastIndexOf(elem);
    });
    if (sums.length === 0) {
      return false;
    } else {
      return true;
    }
  }

  checkMoves() {
    if (this.scoreList.length < this.numberOfMoves) {
      return true;
    } else {
      if (!this.isDraw()) {
        this.winnerEvent.emit(this.getWinner());
        this.gameEvent.emit(false);
      } else {
        if (!this.additionalMoves) {
          this.numberOfMoves += config.additionalMoves;
          this.additionalMoves = true;
          return true;
        } else {
          this.winnerEvent.emit(new Player('DRAW'));
          this.gameEvent.emit(false);
          return false;
        }
      }
    }
  }

  getLastMove() {
    return this.scoreList[this.scoreList.length - 1];
  }

  makeMove() {
    const lastMove = this.getLastMove();
    if (this.checkMoves()) {
      const newMove = new Map<Player, number>();
      const sumOfMove = this.getSumOfMove();
      this.moveScore.forEach((_, key) => {
        if (lastMove.get(key) - sumOfMove.get(key) < 0 || lastMove.get(key) - sumOfMove.get(key) === 1) {
          newMove.set(key, lastMove.get(key));
        } else if (lastMove.get(key) - sumOfMove.get(key) > 0 ) {
          newMove.set(key, lastMove.get(key) - sumOfMove.get(key));
        } else if (lastMove.get(key) - sumOfMove.get(key) === 0) {
          const tempMap = new Map<Player, number>(lastMove);
          Object.values(this.moveItems.get(key)).forEach(
            (el: { points: string; zone: number }) => {
              if (
                tempMap.get(key) - +el.points * el.zone === 0 &&
                el.zone === 2
              ) {
                newMove.set(key, 0);
                this.winnerEvent.emit(key);
                this.gameEvent.emit(false);
                this.isHaveWinner = true;
              } else if (
                tempMap.get(key) - +el.points * el.zone > 0 &&
                !this.isHaveWinner
              ) {
                newMove.set(key, lastMove.get(key));
                tempMap.set(key, tempMap.get(key) - +el.points * el.zone);
              } else if (!this.winnerEvent) {
                newMove.set(key, lastMove.get(key));
              }
            }
          );
        }
      });
      this.scoreList.push(newMove);
    }
  }

  getScoreList() {
    return this.scoreList;
  }
}
