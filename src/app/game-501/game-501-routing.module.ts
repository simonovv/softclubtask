import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Game501Component } from './game-501-layout/game-501.component';

const routes: Routes = [
  {path: '', component: Game501Component}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Game501RoutingModule { }
