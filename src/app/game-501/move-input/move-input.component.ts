import { Component, Input, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Player } from 'src/app/player/player.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Game501Service } from '../game-501.service';

@Component({
  selector: 'app-move-input',
  templateUrl: './move-input.component.html',
  styleUrls: ['./move-input.component.css']
})
export class MoveInputComponent  implements OnInit {
  @Input() player: Player;
  form: FormGroup;
  @Output() changeForm = new EventEmitter<{player: Player, form: FormGroup}>();

  constructor(private gameService: Game501Service) { }

  zones = [
    {value: 1, name: 'x1'},
    {value: 2, name: 'x2'},
    {value: 3, name: 'x3'}
  ];

  ngOnInit() {
    this.form = new FormGroup({
      'Dart1': new FormGroup({
        'points': new FormControl(0, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])),
        'zone': new FormControl(1)
      }),
      'Dart2': new FormGroup({
        'points': new FormControl(0, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])),
        'zone': new FormControl(1)
      }),
      'Dart3': new FormGroup({
        'points': new FormControl(0, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])),
        'zone': new FormControl(1)
      }),
    });
    this.changeForm.emit({player: this.player, form: this.form});
  }

  onChange(dart: string, value: number) {
    this.form.patchValue({
      [dart]: {
        zone: value
      }
    });
    this.gameService.handleMove(this.player, this.form.value);
  }

  handleMove() {
    this.changeForm.emit({player: this.player, form: this.form});
    this.gameService.handleMove(this.player, this.form.value);
  }
}
