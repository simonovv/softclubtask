import { Component, OnInit, Input } from '@angular/core';
import { Player } from 'src/app/player/player.model';

@Component({
  selector: 'app-score-list',
  templateUrl: './score-list.component.html',
  styleUrls: ['./score-list.component.css']
})
export class ScoreListComponent implements OnInit {
  @Input() players: Player[];
  @Input() scoreList: [];

  ngOnInit() {
  }
}
