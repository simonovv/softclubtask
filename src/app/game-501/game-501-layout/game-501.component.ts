import { Component, OnInit } from '@angular/core';
import { Player } from '../../player/player.model';
import { PlayerService } from '../../player/player.service';
import { Game501Service } from '../../game-501/game-501.service';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game-501',
  templateUrl: './game-501.component.html',
  styleUrls: ['./game-501.component.css']
})
export class Game501Component implements OnInit {
  players: Player[];
  scoreList: any[];
  winner: Player;
  isGame: Boolean = false;
  map: Map<Player, FormGroup> = new Map();

  constructor(
    private playerSevice: PlayerService,
    private gameService: Game501Service,
    public translate: TranslateService,
    private router: Router
  ) {
    this.gameService.winnerEvent.subscribe(
      (winner: Player) => this.winner = winner
    );
    this.gameService.gameEvent.subscribe(
      (isGame: boolean) => this.isGame = isGame
    );
  }

  clearInputs() {
    const elements: HTMLCollection = document.getElementsByClassName('radio-group');
    for (let i = 0; i < elements.length; i++) {
      elements[i].children['0'].childNodes['0'].checked = true;
      elements[i].children['1'].childNodes['0'].checked = false;
      elements[i].children['2'].childNodes['0'].checked = false;
    }

    document.querySelectorAll('input.points').forEach((el: any) => {
      el.value = 0;
    });
  }

  newGame() {
    this.clearInputs();
    this.gameService.initGame(this.players);
  }

  ngOnInit() {
    if (this.playerSevice.getAllPlayers().length === 0) {
      this.router.navigate(['dashboard']);
    }
    this.players = this.playerSevice.getAllPlayers();
    this.scoreList = this.gameService.getScoreList();
    this.gameService.initGame(this.players);
  }

  onFormChange(event: {player: Player, form: FormGroup}) {
    this.map.set(event.player, event.form);
    this.checkValidity();
  }

  checkValidity() {
    let valid = true;
    const formArray = Array.from(this.map);
    formArray.forEach(el => {
      valid = valid && el[1].valid;
    });
    return valid;
  }

  makeMove() {
    this.gameService.makeMove();
    this.clearInputs();
  }
}
