import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Game501Component } from './game-501-layout/game-501.component';
import { MoveInputComponent } from './move-input/move-input.component';
import { ScoreListComponent } from './score-list/score-list.component';
import { SharedModule } from '../shared/shared.module';
import { MoveInputHelperDirective } from '../shared/move-input-helper.directive';
import { Game501RoutingModule } from './game-501-routing.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    Game501Component,
    MoveInputComponent,
    ScoreListComponent,
    MoveInputHelperDirective
  ],
  imports: [
    CommonModule,
    SharedModule,
    Game501RoutingModule,
    TranslateModule
  ]
})
export class Game501Module { }
