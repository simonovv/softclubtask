import { Component, Renderer2, OnInit } from '@angular/core';
import { PlayerService } from '../../player/player.service';
import { Router } from '@angular/router';

import routesConfig from '../../../environments/routes';
import { AuthService } from 'src/app/shared/services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  game: string;
  errors = [];
  isLogin = false;
  constructor(
    private playerService: PlayerService,
    private router: Router,
    private renderer: Renderer2,
    private authService: AuthService,
    public translate: TranslateService
  ) { }

  ngOnInit() {
    this.isLogin = this.authService.isAuthunticate();
  }

  onNewPlayer() {
    this.router.navigate(['/' + routesConfig.newPlayer]);
  }

  onSelectGame(event: Event, game: string) {
    this.errors = [];
    this.game = game;
    const gameModes = document.querySelectorAll('.game-mode');
    gameModes.forEach(el => this.renderer.removeClass(el, 'selected-game'));
    this.renderer.addClass(event.target, 'selected-game');
  }

  onStartGame() {
    this.errors = [];
    if (this.playerService.getAllPlayers().length > 0 && this.game) {
      this.router.navigate([this.game]);
    } else {
      if (this.playerService.getAllPlayers().length === 0) {
        this.errors.push('DASHBOARD.ERRORS.NUMBEROFPLAYERS');
      }
      if (!this.game) {
        this.errors.push('DASHBOARD.ERRORS.SELECTGAME');
      }
    }
  }

  onLogin() {
    this.router.navigate(['/login']);
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }
}
