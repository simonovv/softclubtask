import { Component, OnInit } from '@angular/core';
import { Player } from 'src/app/player/player.model';
import { PlayerService } from 'src/app/player/player.service';

@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.css']
})
export class PlayersListComponent implements OnInit {
  players: Player[];

  constructor(private playerService: PlayerService) { }

  ngOnInit() {
    this.players = this.playerService.getAllPlayers();
  }

  onDeletePlayer(index: number) {
    this.playerService.deletePlayer(index);
  }
}
