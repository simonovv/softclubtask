import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard-layout/dashboard.component';
import { PlayersListComponent } from './players-list/players-list.component';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DashboardComponent,
    PlayersListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    TranslateModule
  ]
})
export class DashboardModule { }
