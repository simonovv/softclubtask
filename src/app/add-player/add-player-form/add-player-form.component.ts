import { Component, OnInit } from '@angular/core';
import { PlayerService } from 'src/app/player/player.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Player } from 'src/app/player/player.model';
import { Router } from '@angular/router';

import routesConfig from '../../../environments/routes';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-add-player-form',
  templateUrl: './add-player-form.component.html',
  styleUrls: ['./add-player-form.component.css']
})
export class AddPlayerFormComponent implements OnInit {
  form: FormGroup;
  constructor(private playerService: PlayerService, private router: Router, public translate: TranslateService) { }

  ngOnInit() {
    this.form = new FormGroup({
      'nickname': new FormControl('', Validators.required),
      'email': new FormControl('', Validators.email)
    });
  }

  onSubmit() {
    const { nickname, email } = this.form.value;
    this.playerService.addPlayer(new Player(nickname, email));
    this.router.navigate(['/' + routesConfig.dashboard]);
  }
}
