import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddPlayerComponent } from './add-player-layout/add-player.component';
import { AddPlayerFormComponent } from './add-player-form/add-player-form.component';
import { SharedModule } from '../shared/shared.module';
import { AddPlayerRoutingModule } from './add-player-routing.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddPlayerComponent,
    AddPlayerFormComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AddPlayerRoutingModule,
    TranslateModule
  ]
})
export class AddPlayerModule { }
